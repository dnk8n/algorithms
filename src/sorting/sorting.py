# coding=utf-8
def bubble_sort(a_list):
    a_list_copy = a_list.copy()
    for n in range(len(a_list_copy) - 1, 1, -1):
        for i in range(n):
            if a_list_copy[i] > a_list_copy[i + 1]:
                temp = a_list_copy[i]
                a_list_copy[i] = a_list_copy[i + 1]
                a_list_copy[i + 1] = temp
    return a_list_copy

def short_bubble_sort(a_list):
    a_list_copy = a_list.copy()
    exchanges = True
    n = len(a_list_copy) - 1
    while n > 0 and exchanges:
       exchanges = False
       for i in range(n):
           if a_list_copy[i] > a_list_copy[i + 1]:
               exchanges = True
               temp = a_list_copy[i]
               a_list_copy[i] = a_list_copy[i + 1]
               a_list_copy[i + 1] = temp
       n = n - 1
    return a_list_copy

def selection_sort(a_list):
    a_list_copy = a_list.copy()
    for i in range(len(a_list_copy) - 1):
        min_val = min(a_list_copy[i:])
        min_val_index = a_list_copy.index(min_val)
        if a_list_copy[i] > min_val:
            temp = a_list_copy[i]
            a_list_copy[i] = a_list_copy[min_val_index]
            a_list_copy[min_val_index] = temp
    return a_list_copy

lists_to_sort = [
    [5, 9, 3, 1, 2, 8, 4, 7, 6],
    [54, 26, 93, 17, 77, 31, 44, 55, 20]
]
for list_index, list_to_sort in enumerate(lists_to_sort):
    bubble_sorted = bubble_sort(list_to_sort)
    short_bubble_sorted = short_bubble_sort(list_to_sort)
    selection_sorted = selection_sort(list_to_sort)
    print(f'list_to_sort_{list_index}:             {list_to_sort}')
    print(f'     bubble_sorted_{list_index}:       {bubble_sorted}')
    print(f'     short_bubble_sorted_{list_index}: {short_bubble_sorted}')
    print(f'     selection_sorted_{list_index}:    {selection_sorted}')
    print('')

